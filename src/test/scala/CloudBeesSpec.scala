import org.scalatest.{FlatSpec, Matchers}

/**
  * Created by carloschicas on 2/5/16.
  */
class CloudBeesSpec extends FlatSpec with Matchers {

  "Say my name" should " be Carlos" in {
    val name = "Carlos"
    val say = CloudBees.sayMyName(name)
    assert(say == s"Mi nombre es $name")
    assert(say != s"Mi nombre es Pedro")
  }

  "Suma " should " be a + b" in {
    val a = 2
    val b = 3
    val c = 4
    val sum = CloudBees.sum(a, b)
    assert(sum == a + b)
    assert(sum != a + c)
  }

}
