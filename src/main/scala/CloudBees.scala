/**
  * Created by carloschicas on 2/5/16.
  */
object CloudBees extends App {

  override def main(args: Array[String]): Unit = {
    println("Probando CloudBees")

    println(sayMyName("Carlos"))

    val a = 2
    val b = 2
    val s = sum(a, b)

    println(s"$a + $b = $s")
  }

  def sayMyName(name: String): String = {
    s"Mi nombre es $name"
  }

  def sum(a: Int, b: Int): Int = {
    a + b
  }

}
